﻿// ---------------------------------------------------------------------------------
// ICT - ICT.WPFForm.App
// MainWindowView.cs
// DRNL
// 2016.02.08
// ---------------------------------------------------------------------------------

using ICT.Core.Extensions;
using ICT.Core.Interfaces;
using ICT.WPFForm.App.Interfaces;
using ICT.WPFForm.App.ViewModels;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Windows;

namespace ICT.WPFForm.App.Views
{
    /// <summary>
    /// Interaction logic for MainWindowView.xaml
    /// </summary>
    public partial class MainWindowView : Window, IView, IComposite
    {
        /// <summary>
        /// Main window view.
        /// </summary>
        public MainWindowView()
        {
            // Initialize instace variables
            InitializeComponent();

            // Resolve composition
            if (AppManager == null)
            {
                this.ResolveParts(typeof(MainWindowView));
            }

            // Setup the MVVM framework
            ViewModel = new MainWindowViewModel();
            ViewModel.Model = AppManager.Model;
            DataContext = ViewModel;
        }

        /// <summary>
        /// Main window view model.
        /// </summary>
        private MainWindowViewModel MainWindowViewModel { get; set; }

        /// <summary>
        /// Application manager.
        /// </summary>
        [Import(typeof(IAppManager))]
        public IAppManager AppManager { get; set; }

        /// <summary>
        /// View model.
        /// </summary>
        public IViewModel ViewModel
        {
            get
            {
                return MainWindowViewModel;
            }
            set
            {
                MainWindowViewModel = (MainWindowViewModel)value;
                MainWindowViewModel.PropertyChanged += ViewModelPropertyChanged;
            }
        }

        /// <summary>
        /// View model property changed event handler.
        /// </summary>
        /// <param name="sender">Original sender (view model)</param>
        /// <param name="e">Property</param>
        public void ViewModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // Handle the changes in the properties of the view model
        }
    }
}
