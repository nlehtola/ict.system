﻿// ---------------------------------------------------------------------------------
// ICT - ICT.WPFForm.App
// RelayCommand.cs
// DRNL
// 2016.02.08
// ---------------------------------------------------------------------------------

using ICT.Core.DBC;
using System;
using System.Windows.Input;

namespace ICT.WPFForm.App
{
    public class RelayCommand : ICommand
    {
        /// <summary>
        /// Encapsulated the execute action
        /// </summary>
        private Action<object> ExecuteAction { get; set; }

        /// <summary>
        /// Encapsulated the representation for the validation of the execute method
        /// </summary>
        private Predicate<object> CanExecutePredicate { get; set; }

        /// <summary>
        /// Initializes a new instance of the RelayCommand class
        /// Creates a new command that can always execute.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        public RelayCommand(Action<object> execute)
            : this(execute, DefaultCanExecute)
        {
            // Initialize instance variables
        }

        /// <summary>
        /// Initializes a new instance of the RelayCommand class
        /// Creates a new command.
        /// </summary>
        /// <param name="execute">The execution logic.</param>
        /// <param name="canExecute">The execution status logic.</param>
        public RelayCommand(Action<object> execute, Predicate<object> canExecute)
        {
            // Initialize instance variables
            Contract.Requires(execute != null, "Valid callback");
            Contract.Requires(canExecute != null, "Valid callback");

            ExecuteAction = execute;
            CanExecutePredicate = canExecute;
        }

        /// <summary>
        /// An event to raise when the CanExecute value is changed
        /// </summary>
        /// <remarks>
        /// Any subscription to this event will automatically subscribe to both 
        /// the local OnCanExecuteChanged method AND
        /// the CommandManager RequerySuggested event
        /// </remarks>
        public event EventHandler CanExecuteChanged
        {
            add
            {
                CommandManager.RequerySuggested += value;

                CanExecuteChangedInternal += value;
            }

            remove
            {
                CommandManager.RequerySuggested -= value;

                CanExecuteChangedInternal -= value;
            }
        }

        /// <summary>
        /// An event to allow the CanExecuteChanged event to be raised manually
        /// </summary>
        private event EventHandler CanExecuteChangedInternal;

        /// <summary>
        /// Defines if command can be executed
        /// </summary>
        /// <param name="parameter">the parameter that represents the validation method</param>
        /// <returns>true if the command can be executed</returns>
        public bool CanExecute(object parameter)
        {
            return CanExecutePredicate != null && CanExecutePredicate(parameter);
        }

        /// <summary>
        /// Execute the encapsulated command
        /// </summary>
        /// <param name="parameter">the parameter that represents the execution method</param>
        public void Execute(object parameter)
        {
            ExecuteAction(parameter);
        }

        /// <summary>
        /// Raises the can execute changed.
        /// </summary>
        public void OnCanExecuteChanged()
        {
            var handler = CanExecuteChangedInternal;

            if (handler != null)
            {
                //DispatcherHelper.BeginInvokeOnUIThread(() => handler.Invoke(this, EventArgs.Empty));
                handler.Invoke(this, EventArgs.Empty);
            }
        }

        /// <summary>
        /// Destroys this instance.
        /// </summary>
        public void Destroy()
        {
            CanExecutePredicate = _ => false;
            ExecuteAction = _ => { return; };
        }

        /// <summary>
        /// Defines if command can be executed (default behaviour)
        /// </summary>
        /// <param name="parameter">The parameter.</param>
        /// <returns>Always true</returns>
        private static bool DefaultCanExecute(object parameter)
        {
            return true;
        }
    }
}
