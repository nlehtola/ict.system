﻿// ---------------------------------------------------------------------------------
// ICT - ICT.WPFForm.App
// Model.cs
// DRNL
// 2016.02.08
// ---------------------------------------------------------------------------------

using ICT.Core.Interfaces;
using ICT.Models.Interfaces;
using ICT.WPFForm.App.Properties;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.Composition;
using System.Linq;

namespace ICT.WPFForm.App.Models
{
    /// <summary>
    /// Model class.
    /// </summary>
    [Export(typeof(IModel))]
    internal class TransitModel : IModel, IComposite
    {
        /// <summary>
        /// Static constructor
        /// </summary>
        static TransitModel()
        {
            // Initialize static members
            DefaultConnectorData = Resources.ICTDefaultConnectorData;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        public TransitModel()
        {
            // Initialize instance variables
            ViewModels = new List<IViewModel>();

            ConnectorData = DefaultConnectorData;
        }

        /// <summary>
        /// View models.
        /// </summary>
        private List<IViewModel> ViewModels { get; set; }

        /// <summary>
        /// Connector component
        /// </summary>
        [Import(typeof(IStandardConnector))]
        private IStandardConnector Connector { get; set; }

        /// <summary>
        /// Planner component
        /// </summary>
        [Import(typeof(IPlanner))]
        private IPlanner Planner { get; set; }

        /// <summary>
        /// Default connector data.
        /// </summary>
        private static string DefaultConnectorData { get; set; }

        /// <summary>
        /// Current connector data.
        /// </summary>
        private string ConnectorData { get; set; }

        /// <summary>
        /// Property changed event handler.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Cities.
        /// </summary>
        public List<ICity> Cities 
        {
            get
            {
                // Get cities
                return Connector.GetCities().ToList();
            }
        }

        /// <summary>
        /// Links.
        /// </summary>
        public List<ILink> Links
        {
            get
            {
                // Get links
                return Connector.GetLinks().ToList();
            }
        }

        /// <summary>
        /// Add view model.
        /// </summary>
        /// <param name="viewModel">Given view model</param>
        public void AddViewModel(IViewModel viewModel)
        {
            // Add view model
            ViewModels.Add(viewModel);

            PropertyChanged += viewModel.ModelPropertyChanged;
        }

        /// <summary>
        /// Remove view model.
        /// </summary>
        /// <param name="viewModel">Given view model</param>
        public void RemoveViewModel(IViewModel viewModel)
        {
            // Remove view model
            ViewModels.Remove(viewModel);
        }

        /// <summary>
        /// Notify that a property has changed.
        /// </summary>
        /// <param name="propertyName">Name of the property</param>
        public void NotifyPropertyChanged(string propertyName)
        {
            // Propagate changes to the view model
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        /// <summary>
        /// Initialize.
        /// </summary>
        public void Initialize()
        {
            try
            {
                // Planner setup
                Planner.Setup(Connector);

                // Connector update
                Connector.Update(ConnectorData, new TransitModelConnectorParser());
            }
            catch
            {
                // Force to terminate...
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Reset.
        /// </summary>
        public void Reset()
        {
            try
            {
                // Planner setup
                Planner.Setup(Connector);

                // Connector update
                Connector.Update(ConnectorData, new TransitModelConnectorParser());

                // Notify
                NotifyPropertyChanged("Network");
            }
            catch
            {
                // Force to terminate...
                Environment.Exit(0);
            }
        }

        /// <summary>
        /// Clear.
        /// </summary>
        public void Clear()
        {
            try
            {
                // Clear connector data
                ConnectorData = string.Empty;

                // Connector reset 
                Connector.Reset();

                // Notify
                NotifyPropertyChanged("Network");
            }
            catch
            {
                // Force to terminate...
                Environment.Exit(0);
            }
        }
    }
}
