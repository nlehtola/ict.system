﻿// ---------------------------------------------------------------------------------
// ICT - ICT.Core
// IAppManager.cs
// DRNL
// 2016.02.08
// ---------------------------------------------------------------------------------

using ICT.Core.Interfaces;
using ICT.WPFForm.App.Interfaces;
using ICT.WPFForm.App.Models;
using System.ComponentModel.Composition;

namespace ICT.WPFForm.App
{
    /// <summary>
    /// Application manager interface.
    /// </summary>
    [PartCreationPolicy(CreationPolicy.NonShared)]
    [Export(typeof(IAppManager))]
    public class AppManager : IAppManager
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public AppManager()
        {
            // Initialize instance variables
        }

        /// <summary>
        /// Model.
        /// </summary>
        [Import(typeof(IModel))]
        public IModel Model { get; set; }

        /// <summary>
        /// Initialize the application and its resources.
        /// </summary>
        public void Initialize()
        {
            // Initialize the transit model
            TransitModel.Initialize();
        }

        #region PrivateMembers

        /// <summary>
        /// Transit model.
        /// </summary>
        private TransitModel TransitModel 
        {
            get
            {
                return Model as TransitModel;
            }
        }

        #endregion
    }
}
