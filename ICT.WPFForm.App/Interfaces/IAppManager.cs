﻿// ---------------------------------------------------------------------------------
// ICT - ICT.Core
// IAppManager.cs
// DRNL
// 2016.02.08
// ---------------------------------------------------------------------------------

using ICT.Core.Interfaces;

namespace ICT.WPFForm.App.Interfaces
{
    /// <summary>
    /// Application manager interface.
    /// </summary>
    public interface IAppManager
    {
        /// <summary>
        /// Model.
        /// </summary>
        IModel Model { get; set; }

        /// <summary>
        /// Initialize the application and its resources.
        /// </summary>
        void Initialize();
    }
}
