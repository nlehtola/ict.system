﻿using ICT.Core.Extensions;
using ICT.Core.Interfaces;
using ICT.WPFForm.App.Interfaces;
using System.ComponentModel.Composition;
using System.Windows;

namespace ICT.WPFForm.App
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application, IComposite
    {
        /// <summary>
        /// Constructor.
        /// </summary>
        public App()
        {
            // Resolve composition
            if (AppManager == null)
            {
                this.ResolveParts(typeof(App));
            }

            // Initialize the application manager
            AppManager.Initialize();
        }

        /// <summary>
        /// Application manager.
        /// </summary>
        [Import(typeof(IAppManager))]
        public IAppManager AppManager { get; set; }
    }
}
