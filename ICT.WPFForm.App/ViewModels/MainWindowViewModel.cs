﻿// ---------------------------------------------------------------------------------
// ICT - ICT.WPFForm.App
// MainWindowViewModel.cs
// DRNL
// 2016.02.08
// ---------------------------------------------------------------------------------

using ICT.Core.Interfaces;
using ICT.WPFForm.App.Models;
using System.ComponentModel;
using System.Windows.Input;

namespace ICT.WPFForm.App.ViewModels
{
    /// <summary>
    /// Main window view model.
    /// </summary>
    public class MainWindowViewModel : IViewModel
    {
        /// <summary>
        /// Cosntructor.
        /// </summary>
        public MainWindowViewModel()
        {
            // Initizliaze instance variables
            NavigateHomeCommand = new RelayCommand(NavigateHome);
            DisplayCurrentCitiesCommand = new RelayCommand(DisplayCurrentCities);
            DisplayCurrentLinksCommand = new RelayCommand(DisplayCurrentLinks);
            ResetConnectorDataCommand = new RelayCommand(ResetConnectorData);
            ClearConnectorDataCommand = new RelayCommand(ClearConnectorData);
            AddLinkCommand = new RelayCommand(AddLink);
            CheckRouteCommand = new RelayCommand(CheckRoute);
            CalculateRouteDistanceCommand = new RelayCommand(CalculateRouteDistance);
            GetRouteMinimumDistanceCommand = new RelayCommand(GetRouteMinimumDistance);
            GetAllRoutesStopConstraintCommand = new RelayCommand(GetAllRoutesStopConstraint);
            GetAllRoutesDistanceConstraintCommand = new RelayCommand(GetAllRoutesDistanceConstraint);
        }

        /// <summary>
        /// Model.
        /// </summary>
        private TransitModel TransitModel { get; set; }

        /// <summary>
        /// Home command.
        /// </summary>
        private ICommand NavigateHomeCommand { get; set; }

        /// <summary>
        /// Display current cities command.
        /// </summary>
        private ICommand DisplayCurrentCitiesCommand { get; set; }

        /// <summary>
        /// Display current links command.
        /// </summary>
        private ICommand DisplayCurrentLinksCommand { get; set; }

        /// <summary>
        /// Clear connector data command.
        /// </summary>
        private ICommand ClearConnectorDataCommand { get; set; }

        /// <summary>
        /// Reset connector data command.
        /// </summary>
        private ICommand ResetConnectorDataCommand { get; set; }

        /// <summary>
        /// Add link command.
        /// </summary>
        private ICommand AddLinkCommand { get; set; }

        /// <summary>
        /// Check route command.
        /// </summary>
        private ICommand CheckRouteCommand { get; set; }

        /// <summary>
        /// Calculate route distance command.
        /// </summary>
        private ICommand CalculateRouteDistanceCommand { get; set; }

        /// <summary>
        /// Get route minimum distance command.
        /// </summary>
        private ICommand GetRouteMinimumDistanceCommand { get; set; }

        /// <summary>
        /// Get all routes with stop constraint command.
        /// </summary>
        private ICommand GetAllRoutesStopConstraintCommand { get; set; }

        /// <summary>
        /// Get all routes with distance constraint command.
        /// </summary>
        private ICommand GetAllRoutesDistanceConstraintCommand { get; set; }

        /// <summary>
        /// Model.
        /// </summary>
        public IModel Model
        {
            get
            {
                return TransitModel;
            }
            set
            {
                TransitModel = (TransitModel)value;
                TransitModel.PropertyChanged += ModelPropertyChanged;
            }
        }

        /// <summary>
        /// Property changed event handler.
        /// </summary>
        public event PropertyChangedEventHandler PropertyChanged;

        /// <summary>
        /// Model property changed event hanlder.
        /// </summary>
        /// <param name="sender">Original sender (model)</param>
        /// <param name="e">Property</param>
        public void ModelPropertyChanged(object sender, PropertyChangedEventArgs e)
        {
            // Handle the changes in the properties of the model
            var propertyName = e.PropertyName.ToLower();

            if (propertyName.Equals("network"))
            {
            }
        }

        /// <summary>
        /// Notify that a property has changed.
        /// </summary>
        /// <param name="propertyName">Name of the property</param>
        public void NotifyPropertyChanged(string propertyName)
        {
            // Propagate changes to the view
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
            }
        }

        #region PrivateMembers

        /// <summary>
        /// Navigate home.
        /// </summary>
        /// <param name="obj">Parameter</param>
        private void NavigateHome(object obj)
        {
        }

        /// <summary>
        /// Display current cities.
        /// </summary>
        /// <param name="obj">Parameter</param>
        private void DisplayCurrentCities(object obj)
        {
        }

        /// <summary>
        /// Display current links.
        /// </summary>
        /// <param name="obj">Parameter</param>
        private void DisplayCurrentLinks(object obj)
        {
        }

        /// <summary>
        /// Reset connector data.
        /// </summary>
        /// <param name="obj">Parameter</param>
        private void ResetConnectorData(object obj)
        {
            // Reset connector data
            TransitModel.Reset();
        }

        /// <summary>
        /// Clear connector data.
        /// </summary>
        /// <param name="obj">Parameter</param>
        private void ClearConnectorData(object obj)
        {
            // Clear connector data
            TransitModel.Clear();
        }

        /// <summary>
        /// Add link.
        /// </summary>
        /// <param name="obj">Parameter</param>
        private void AddLink(object obj)
        {
        }

        /// <summary>
        /// Check route.
        /// </summary>
        /// <param name="obj">Parameter</param>
        private void CheckRoute(object obj)
        {
        }

        /// <summary>
        /// Calculate route distance.
        /// </summary>
        /// <param name="obj">Parameter</param>
        private void CalculateRouteDistance(object obj)
        {
        }
        
        /// <summary>
        /// Get route minimum distance.
        /// </summary>
        /// <param name="obj">Parameter</param>
        private void GetRouteMinimumDistance(object obj)
        {
        }

        /// <summary>
        /// Get all routes with stop constraint.
        /// </summary>
        /// <param name="obj">Parameter</param>
        private void GetAllRoutesStopConstraint(object obj)
        {
        }

        /// <summary>
        /// Get all routes with distance constraint.
        /// </summary>
        /// <param name="obj">Parameter</param>
        private void GetAllRoutesDistanceConstraint(object obj)
        {
        }

        #endregion
    }
}
