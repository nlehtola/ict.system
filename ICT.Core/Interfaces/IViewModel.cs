﻿// ---------------------------------------------------------------------------------
// ICT - ICT.Core
// IModel.cs
// DRNL
// 2016.02.08
// ---------------------------------------------------------------------------------

using ICT.Core.Interfaces;
using System.ComponentModel;

namespace ICT.Core.Interfaces
{
    /// <summary>
    /// Interface for the MVVM view model.
    /// </summary>
    public interface IViewModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Model.
        /// </summary>
        IModel Model { get; set; }

        /// <summary>
        /// Property changed event handler
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ModelPropertyChanged(object sender, PropertyChangedEventArgs e);

        /// <summary>
        /// Notify that property has changed.
        /// </summary>
        /// <param name="property">Name of the property</param>
        void NotifyPropertyChanged(string property);
    }
}
