﻿// ---------------------------------------------------------------------------------
// ICT - ICT.Core
// IModel.cs
// DRNL
// 2016.02.08
// ---------------------------------------------------------------------------------

using System.ComponentModel;

namespace ICT.Core.Interfaces
{
    /// <summary>
    /// Interface for the MVVM view model.
    /// </summary>
    public interface IView
    {
        /// <summary>
        /// View model.
        /// </summary>
        IViewModel ViewModel { get; set; }

        /// <summary>
        /// Property changed event handler.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        void ViewModelPropertyChanged(object sender, PropertyChangedEventArgs e);
    }
}
