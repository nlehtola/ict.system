﻿// ---------------------------------------------------------------------------------
// ICT - ICT.Core
// IModel.cs
// DRNL
// 2016.02.08
// ---------------------------------------------------------------------------------

using System.ComponentModel;

namespace ICT.Core.Interfaces
{
    /// <summary>
    /// Interface for the MVVM model.
    /// </summary>
    public interface IModel : INotifyPropertyChanged
    {
        /// <summary>
        /// Add the given view model to the view model collection and, as well as, 
        /// register it.
        /// </summary>
        /// <param name="viewModel">Given view model</param>
        void AddViewModel(IViewModel viewModel);

        /// <summary>
        /// Remove the given view model from the view model collection.
        /// </summary>
        /// <param name="viewModel">Given view model</param>
        void RemoveViewModel(IViewModel viewModel);

        /// <summary>
        /// Notify that a property has changed.
        /// </summary>
        /// <param name="propertyName">Name of the property</param>
        void NotifyPropertyChanged(string propertyName);
    }
}
